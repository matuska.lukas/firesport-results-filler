const fs = require('fs')
const axios = require('axios')
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

const config = JSON.parse(fs.readFileSync('config.json'))
const instance = axios.create({
  baseURL: `${config.destination.protocol}://${config.destination.host}:${config.destination.port}/api`,
  timeout: 1000,
  headers: { 'X-Custom-Header': 'foobar' }
})

function findTeamByName (name) {
  instance
    .get('/teams/find-by-name', {
      params: {
        name: name
      }
    }).then((res) => {
      console.log(res.data.name)
      if (res.data.name === undefined) {
        console.log(`Name from JSON: '${name}'`)
        console.log('Team wasn\'t find in database, so we can try another name:')
        readline.question('Team\'s name: ', name => {
          findTeamByName(name)
          readline.close()
        })
      } else {
        global.results[global.ii].team = res.data._id
        global.ii++
        if (global.ii < global.results.length) {
          findTeamByName(global.results[global.ii].team.name)
        } else {
          console.log('Teams are done successfully.')
          // console.log(global.results)
          process.exit(0)
        }
      }
    }, (error) => {
      console.error(error)
    })
}

for (let i = 0; i < config.source.urls.length; i++) {
  const filename = config.source.urls[i].split('/')[config.source.urls[i].split('/').length - 1].split('.html')[0] + '.json'
  global.results = JSON.parse(fs.readFileSync(filename))
  global.ii = 0
  findTeamByName(global.results[global.ii].team.name)
}
