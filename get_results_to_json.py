import requests
import json
from bs4 import BeautifulSoup

def getTargetResult(time):
    if time == 'NP' or time == '-':
        return 0
    elif time == 'D':
        return -1
    else:
        return time

def getResultsFromUrl(URL):
    rows = BeautifulSoup(requests.get(URL).content, 'html.parser').find(id='tab1151').find_all('tr')
    resultsForRequest = []

    for result in rows:
        links = result.find_all('td')
        resultForRequest = {
            'team': {},
            'targets': {},
            'media': {}
        }
        if len(links) != 0:
            for i in range(len(links)):
                linkText = links[i].text.strip()
                if linkText != '' or linkText != None:
                    if i == 1:
                        resultForRequest['team']['name'] = linkText
                    elif i == 2:
                        resultForRequest['team']['district'] = linkText
                    elif i == 3:
                        resultForRequest['targets']['finalTime'] = getTargetResult(linkText)
                    elif i == 4:
                        resultForRequest['targets']['left'] = getTargetResult(linkText)
                    elif i == 5:
                        resultForRequest['targets']['right'] = getTargetResult(linkText)
                    elif i == 6:
                        resultForRequest['media']['youtube'] = linkText
            if resultForRequest['media'] == {} or resultForRequest['media']['youtube'] == '':
                del resultForRequest['media']
            resultsForRequest.append(resultForRequest)
    return resultsForRequest

def resultsToJson(results, filename):
    with open('{}.json'.format(filename), 'w', encoding='utf-8') as f:
        json.dump(results, f)

if __name__ == '__main__':
    with open("config.json") as f:
        CONFIG = json.load(f)

    for url in CONFIG['source']['urls']:
        resultsToJson(getResultsFromUrl(url), url.split('/')[-1].split('.html')[0])
    print('All tasks done.')